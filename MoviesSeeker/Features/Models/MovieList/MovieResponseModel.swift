//
//  MovieResponseModel.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import Foundation

struct MovieResponseModel: Decodable {
    
    let page: Int?
    let totalResults: Int64?
    let totalPages: Int64?
    let movies: [Movie]?
    
    private enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case movies = "results"
    }
}
