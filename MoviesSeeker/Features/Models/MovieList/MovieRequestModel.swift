//
//  MovieRequestModel.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import Foundation

struct MovieRequestModel: Encodable {
    
    let page: Int
    let api_key: String
}
