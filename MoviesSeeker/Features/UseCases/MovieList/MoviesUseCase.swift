//
//  MoviesUseCase.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import RxSwift

protocol MoviesUseCase {
    func getMovies(for page: Int) -> Single<MovieResponseModel>
}

final class NowPlayingMoviesUseCase {
    
    let networking: Networking
    
    init(networking: Networking = NetworkManager()) {
        self.networking = networking
    }
}

extension NowPlayingMoviesUseCase: MoviesUseCase {
    
    func getMovies(for page: Int) -> Single<MovieResponseModel> {
        return .create(subscribe: { single -> Disposable in
            // request building
            let parameters = MovieRequestModel(page: page, api_key: Constants.Keys.api)
            let request = RequestBuilder(path: .init(endPoint: .movie), parameters: parameters)
            // perform action to get
            let task = self.networking.get(request: request, completion: { (response: APIResponse<MovieResponseModel>) in
                switch response.result {
                case .failure(let error): single(.error(error))
                case .success(let value): single(.success(value))
                }
            })
            
            return Disposables.create {
                task.cancel()
            }
        })
    }
}
