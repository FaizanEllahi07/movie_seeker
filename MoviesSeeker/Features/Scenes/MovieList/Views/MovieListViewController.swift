//
//  MovieListViewController.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MovieListViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var nextPageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var movieTableView: UITableView!
    
    //MARK: Variables
    //Injected Properties
    var viewModel: MovieListViewModel!
    var imageLoader: ImageLoader!
    
    //Private Properties
    private let disposeBag = DisposeBag()
    private let viewDidLoadSubject = PublishSubject<Void>()
    private let scrollToEndSubject = PublishSubject<Void>()
    private lazy var datePickerViewer = DatePickerPresenter()
    private lazy var filterBarButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        return button
    }()

    //MARK: Controller Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
        
        //Emit signal on viewDidLoad triggered
        viewDidLoadSubject.onNext(())
    }
    
    //MARK: - Private Methods
    private func setupUI() {
        title = "Movies"
        navigationItem.rightBarButtonItem = filterBarButton
    }
    
    //MARK: - Actions Methods
    func showDatePicker() {
        datePickerViewer.present(into: view)
    }
    
    private func bindViewModel() {
        //setup input for View
        let input = MovieListViewModel.Input(
            viewDidLoad: viewDidLoadSubject.asSignal(onErrorJustReturn: ()),
            scrollingDidEnd: scrollToEndSubject.asSignal(onErrorJustReturn: ()),
            dateFilterApplied: datePickerViewer.dateDidSelectSubject.asSignal(onErrorJustReturn: Date()),
            filterDidTap: filterBarButton.rx.tap.asSignal(),
            movieDidSelectAtIndex: movieTableView.rx.itemSelected.map({ $0.row }).asDriverOnErrorJustComplete())
        
        //transform input to output
        let output = viewModel.transform(input: input)
        
        //setup Output to View
        [output.reloadTableView.emit(onNext: movieTableView.reloadData),
         output.fetching.drive(nextPageActivityIndicator.rx.isAnimating),
         output.filterTitle.emit(to: filterBarButton.rx.title),
         output.showDatePicker.emit(onNext: showDatePicker),
         output.error.drive(onNext: UIAlertController.showAlert),
         output.movieDidSelect.drive()
            ]
            .forEach({ $0.disposed(by: disposeBag) })
    }
}


//MARK: Tableview Data Source
extension MovieListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MovieTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        //Get view model for cell
        let cellViewModel = viewModel[movieViewModelAtIndex: indexPath.row]
        imageLoader.loadImage(with: cell.posterImageView, withURL: cellViewModel.imageURL)
        cell.configure(with: cellViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let isLastRow = indexPath.row == viewModel.numberOfRows - 1
        
        if isLastRow {
            scrollToEndSubject.onNext(())
        }
    }
}
