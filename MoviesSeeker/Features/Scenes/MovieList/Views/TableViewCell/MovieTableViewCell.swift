//
//  MovieTableViewCell.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit


class MovieTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!

    //MARK: Awake From Nib
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK: Setup UI
    
    /// Rounded rating label
    private func setupUI() {
        posterImageView?.layer.cornerRadius = 4.0
        ratingLabel?.layer.masksToBounds = true
        ratingLabel?.layer.cornerRadius = 20.0
    }
}

extension MovieTableViewCell {
    func configure(with viewModel: MovieViewModel) {
        titleLabel.text = viewModel.title
        releaseDateLabel.text = viewModel.releaseDate
        overviewLabel.text = viewModel.overview
        ratingLabel.text = viewModel.rating
    }
}
