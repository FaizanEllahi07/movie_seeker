//
//  MovieListNavigator.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

protocol MovieListNavigator {
    func navigateToDatail(with movie: Movie)
}

class DefaultMovieListNavigator: MovieListNavigator {
    
    private weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func navigateToDatail(with movie: Movie) {
        // controller create & setup
        let storyboard = UIStoryboard(storyboard: .movieDetail)
        let movieDetailController: MovieDetailViewController = storyboard.initialViewController()
        
        //View Model create & setup
        movieDetailController.viewModel = MovieDetailViewModel(movie: movie)
        
        //lazy loader create & setup
        movieDetailController.imageLoader = KingfisherImageLoader()
        
        navigationController?.pushViewController(movieDetailController, animated: true)
    }
}
