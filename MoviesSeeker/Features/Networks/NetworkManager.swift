//
//  NetworkManager.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import Foundation
import Alamofire

struct DefaultAPIRequest: APIRequest {
    
    let request: DataRequest
    func cancel() {
        request.cancel()
    }
}

class NetworkManager: Networking {
    
    func get<T: Decodable, R: Encodable>(request: RequestBuilder<R>, completion: @escaping Completion<T>) -> APIRequest {
        
        return dispatch(url: request.path.url,
                        method: .get,
                        parameters: request.parameters,
                        encoder: URLEncodedFormParameterEncoder.default,
                        headers: request.headers,
                        completion: completion)
        
    }
    
    func dispatch<T: Decodable, R: Encodable>(
        url: URLConvertible,
        method: HTTPMethod,
        parameters: R?,
        encoder: ParameterEncoder,
        headers: [String: String]?,
        completion: @escaping Completion<T>) -> APIRequest {
        
        let headers: HTTPHeaders? = headers.map({ HTTPHeaders($0) })
        
        let dataRequest = AF.request(url,
                                     method: method,
                                     parameters: parameters,
                                     encoder: encoder,
                                     headers: headers)
            .validate()
            .responseDecodable { (response: DataResponse<T>) in
                completion(APIResponse(result: response.result))
        }
        
        return DefaultAPIRequest(request: dataRequest)
    }
}
