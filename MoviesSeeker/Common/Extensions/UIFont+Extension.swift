//
//  UIFont+Extension.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

extension UIFont {
    
    convenience init(_ name: FontName, size: FontSize) {
        self.init(name: name.rawValue, size: size.value)!
    }
    
    enum FontSize {
        case standard(StandardSize)
        case custom(CGFloat)
        var value: CGFloat {
            switch self {
            case .standard(let size):
                return size.rawValue
            case .custom(let customSize):
                return customSize
            }
        }
    }
    
    enum FontName: String {
        case avenirDemiBold = "AvenirNextCondensed-DemiBold"
    }
    
    enum StandardSize: CGFloat {
        case h1 = 24.0
        case h2 = 22.0
        case h3 = 20.0
        case h4 = 18.0
        case h5 = 16.0
        case h6 = 14.0
    }
}
