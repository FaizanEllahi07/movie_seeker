//
//  UIStoryboard+Extension.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    enum StoryboardName: String {
        case movieList = "MovieList"
        case movieDetail = "MovieDetail"
    }
    
    // MARK: Convenience Initializers
    /// Convenience Initializers
    convenience init(storyboard: StoryboardName, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle:bundle)
    }
    
    /// View Controller Instantiation from Generics
    func initialViewController<T: UIViewController>() -> T {
        guard let viewController = instantiateInitialViewController() as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.self) ")
        }
        return viewController
    }
}
