//
//  UITableView+Extension.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

extension UITableView {
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unable to Dequeue Reusable Table View Cell")
        }
        return cell
    }
}
