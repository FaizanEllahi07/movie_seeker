//
//  UIKit+ReuseIdentifier.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

protocol ReuseIdentifying {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifying {
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
}

extension UITableViewCell: ReuseIdentifying {}

