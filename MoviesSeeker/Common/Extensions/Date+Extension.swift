//
//  Date+Extension.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import Foundation

extension Date {
    
    enum StringFormat: String {
        case standard = "yyyy-MM-dd"
    }
    
    func string(with format: StringFormat = .standard) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        formatter.dateFormat = format.rawValue
        return formatter.string(from: self)
    }
}
