//
//  ImageLoader.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit
import Kingfisher

protocol ImageLoader {
    func loadImage(with imageView: UIImageView?, withURL url: String?)
}

struct KingfisherImageLoader: ImageLoader {
    
    func loadImage(with imageView: UIImageView?, withURL url: String?) {
        let placeholderImage = #imageLiteral(resourceName: "placeholder")
        imageView?.image = placeholderImage
        
        guard let stringURL = url else { return }
        
        let resource = URL(string: stringURL)
        imageView?.kf.setImage(with: resource, placeholder: placeholderImage, options: [.transition(.fade(0.25))])
    }
}
