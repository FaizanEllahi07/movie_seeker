//
//  AppNavigator.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

class AppNavigator {
    
    func installRoot(into window: UIWindow?) {
        // Movie List Controller create & setup
        let storyboard = UIStoryboard(storyboard: .movieList)
        let movieListController: MovieListViewController = storyboard.initialViewController()
        let rootController = AppNavigationController(rootViewController: movieListController)
        
        //View Model create & setup
        movieListController.viewModel = MovieListViewModel(
            moviesUseCase: NowPlayingMoviesUseCase(),
            navigator: DefaultMovieListNavigator(navigationController: rootController))
        
        //lazy loader create & setup
        movieListController.imageLoader = KingfisherImageLoader()
        
        window?.rootViewController = rootController
    }
}
