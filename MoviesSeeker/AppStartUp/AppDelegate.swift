//
//  AppDelegate.swift
//  MoviesSeeker
//
//  Created by Faizan Ellahi on 5/8/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy var navigator = AppNavigator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        navigator.installRoot(into: window)
        window?.makeKeyAndVisible()
        return true
    }
}

