//
//  MockMoviesUseCase.swift
//  MoviesSeekerTests
//
//  Created by Faizan Ellahi on 5/10/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import XCTest
import RxSwift

@testable import MoviesSeeker

class MockMoviesUseCase: XCTestCase {
    
    let networking =  MockNetworking()
    var nowPlayingMoviesUseCase: NowPlayingMoviesUseCase!
    
    override func setUp() {
       nowPlayingMoviesUseCase  = NowPlayingMoviesUseCase(networking: networking)
    }
    
    func testGetMovies() {
        nowPlayingMoviesUseCase.getMovies(for: 1).subscribe(onSuccess: { (response) in
            XCTAssertNotNil(response)
            XCTAssertNotNil(response.movies, "Movies List Not Found")
        }) { (error) in
            XCTAssertNil(error)
        }.dispose()
    }
    
    func testMoviesData() {
        nowPlayingMoviesUseCase.getMovies(for: 1).subscribe(onSuccess: { (response) in
            XCTAssertNotNil(response)
            XCTAssertEqual(response.movies?.first?.adult, false)
            XCTAssertEqual(response.movies?.first?.originalTitle, "Avengers: Endgame")
            XCTAssertEqual(response.movies?.first?.id, 299534)
            XCTAssertEqual(response.movies?.first?.originalLanguage, "en")
            XCTAssertEqual(response.movies?.first?.posterPath, "/or06FN3Dka5tukK1e9sl16pB3iy.jpg")
        }) { (error) in
            XCTAssertNil(error)
            }.dispose()
    }
}
