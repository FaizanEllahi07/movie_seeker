//
//  MockNetworking.swift
//  MoviesSeekerTests
//
//  Created by Faizan Ellahi on 5/10/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import Foundation
import RxSwift

@testable import MoviesSeeker

class MockNetworking: Networking {
    
    func get<T, R>(request: RequestBuilder<R>, completion: @escaping (APIResponse<T>) -> Void) -> APIRequest where T : Decodable, R : Encodable {
        
        let bundle = Bundle.init(for: MockNetworking.self)
        if let path = bundle.path(forResource: "Movies", ofType: "json") {
            if let mockResponse: String = try? String(contentsOf: URL(fileURLWithPath: path)) {
                let f =  try! JSONDecoder().decode(T.self, from: mockResponse.data(using: String.Encoding.utf8)!)
                
                completion(APIResponse(result: Result<T, Error>.success(f)))
            }
        }
        return MockAPIRequest()
    }
}

struct MockAPIRequest: APIRequest {
    func cancel() {
        
    }
}
