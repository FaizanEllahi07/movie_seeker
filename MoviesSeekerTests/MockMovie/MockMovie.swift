//
//  MockMovie.swift
//  MoviesSeekerTests
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

@testable import MoviesSeeker

extension Movie {
    init(
        originalTitle: String?,
        backdropPath: String?,
        posterPath: String?,
        overview: String?,
        releaseDate: String?
        ) {
        self.init(id: 123,
                  title: nil,
                  voteCount: nil,
                  video: nil,
                  voteAverage: nil,
                  popularity: nil,
                  posterPath: posterPath,
                  originalLanguage: nil,
                  originalTitle: originalTitle,
                  backdropPath: backdropPath,
                  adult: nil,
                  overview: overview,
                  releaseDate: releaseDate,
                  genreIds: nil)
    }
}
