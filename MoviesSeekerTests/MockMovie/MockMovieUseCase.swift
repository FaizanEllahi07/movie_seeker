//
//  MockMovieUseCase.swift
//  MoviesSeekerTests
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import RxSwift
@testable import MoviesSeeker


class MockMovieUseCase: MoviesUseCase {
    var pageNo = 0
    func getMovies(for page: Int) -> Single<MovieResponseModel> {
        pageNo = page
        let movie1 = Movie(originalTitle: "movie1", backdropPath: "", posterPath: "", overview: "overview1", releaseDate: "12-12-1998")
        let movie2 = Movie(originalTitle: "movie1", backdropPath: "", posterPath: "", overview: "overview1", releaseDate: "12-12-1998")
        let movie3 = Movie(originalTitle: "movie1", backdropPath: "", posterPath: "", overview: "overview1", releaseDate: "12-12-1998")
        let response = MovieResponseModel(page: 1, totalResults: 3, totalPages: 10, movies: [movie1,movie2,movie3])
        
        return .just(response)
    }
    
}
