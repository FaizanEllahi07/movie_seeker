//
//  MovieListTests.swift
//  MoviesSeekerTests
//
//  Created by Faizan Ellahi on 5/9/19.
//  Copyright © 2019 FaizanEllahi. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa

@testable import MoviesSeeker

func CreateMovieListViewModelInput(viewDidLoad: Signal<Void> = .empty(),
                                   scrollingDidEnd: Signal<Void> = .empty(),
                                   dateFilterApplied: Signal<Date> = .empty(),
                                   filterDidTap: Signal<Void> = .empty(),
                                   movieDidSelectAtIndex: Driver<Int> = .empty()
    ) -> MovieListViewModel.Input {
    
    return MovieListViewModel.Input(viewDidLoad: viewDidLoad,
                                    scrollingDidEnd: scrollingDidEnd,
                                    dateFilterApplied: dateFilterApplied,
                                    filterDidTap: filterDidTap,
                                    movieDidSelectAtIndex: movieDidSelectAtIndex)
}

class MockMovieListNavigator: MovieListNavigator {
    var isNavigate = false
    func navigateToDatail(with movie: Movie) {
        isNavigate = true
    }
}

class MovieListTests: XCTestCase {
    
    var mockNavigator: MockMovieListNavigator!
    var mockMovieUseCase: MockMovieUseCase!
    var viewModel: MovieListViewModel!

    override func setUp() {
        mockNavigator = MockMovieListNavigator()
        mockMovieUseCase = MockMovieUseCase()
        viewModel = MovieListViewModel(moviesUseCase: mockMovieUseCase, navigator: mockNavigator)
    }

    override func tearDown() {
        mockNavigator = nil
        mockMovieUseCase = nil
        viewModel = nil
    }
    
    func testReloadTableViewOnViewDidLoad() {
        var isReloaded = false
        var title: String = ""
        var busyIndicatorShown = false
        
        autoreleasepool {
            let viewDidLoad = PublishSubject<Void>()
            let input = CreateMovieListViewModelInput(viewDidLoad: viewDidLoad.asSignal(onErrorJustReturn: ()))
            
            
            let output = viewModel.transform(input: input)
            
            _ = output.reloadTableView.emit(onNext: { _ in
                isReloaded = true
            })
            
            _ = output.filterTitle.emit(onNext: { actionTitle in
                title = actionTitle
            })
            
            _ = output.fetching.drive(onNext: { isFetching in
                busyIndicatorShown = true
            })
            
            viewDidLoad.onNext(())
            let movieViewModel: Any = viewModel[movieViewModelAtIndex: 0]
            XCTAssert(viewModel.numberOfRows == 3)
            XCTAssertTrue(movieViewModel is MovieViewModel)
        }
        XCTAssertTrue(isReloaded)
        XCTAssertTrue(title == "Filter")
        XCTAssertTrue(busyIndicatorShown)
    }
    
    func testReloadTableViewOnNextPageTriggered() {
        var isReloaded = false
        
        autoreleasepool {
            let viewDidLoad = PublishSubject<Void>()
            let nextPageSubject = PublishSubject<Void>()
            
            let input = CreateMovieListViewModelInput(viewDidLoad: viewDidLoad.asSignal(onErrorJustReturn: ()),
                                                      scrollingDidEnd: nextPageSubject.asSignal(onErrorJustReturn: ()))
            
            let output = viewModel.transform(input: input)
            
            _ = output.reloadTableView.emit(onNext: { _ in
                isReloaded = true
            })
            
            viewDidLoad.onNext(())
            XCTAssertTrue(mockMovieUseCase.pageNo == 1)
            nextPageSubject.onNext(())
            XCTAssertTrue(mockMovieUseCase.pageNo == 2)
            XCTAssert(viewModel.numberOfRows == 6)
        }
        XCTAssertTrue(isReloaded)
    }
    
    func testShowDatePickerOnFilterAction() {
        var isDatePickerShown = false
        
        autoreleasepool {
            let filterAction = PublishSubject<Void>()
            let input = CreateMovieListViewModelInput(filterDidTap: filterAction.asSignal(onErrorJustReturn: ()))
            
            let output = viewModel.transform(input: input)
            
            
            _ = output.showDatePicker.emit(onNext: { _ in
                isDatePickerShown = true
            })
            
            filterAction.onNext(())
            
        }
        XCTAssertTrue(isDatePickerShown)
    }
    
    func testFilterForDateSelectionAndResetAction() {
        var isDatePickerShown = false
        var actionTitle: String = ""
        
        autoreleasepool {
            let dateAction = PublishSubject<Date>()
            let viewDidLoad = PublishSubject<Void>()
            let tapAction = PublishSubject<Void>()
            let input = CreateMovieListViewModelInput(
                viewDidLoad: viewDidLoad.asSignal(onErrorJustReturn: ()),
                dateFilterApplied: dateAction.asSignal(onErrorJustReturn: Date()),
                filterDidTap: tapAction.asSignal(onErrorJustReturn: ())
            )
            
            let output = viewModel.transform(input: input)
            
            
            _ = output.showDatePicker.emit(onNext: { _ in
                isDatePickerShown = true
            })
            
            _ = output.filterTitle.emit(onNext: { title in
                actionTitle = title
            })
            
            _ = output.reloadTableView.emit()
            
            viewDidLoad.onNext(())
            XCTAssertTrue(viewModel.numberOfRows == 3)
            
            //filter applied
            dateAction.onNext(Date())
            XCTAssertTrue(actionTitle == "Reset")
            XCTAssertTrue(viewModel.numberOfRows == 0)
            
            //reset filter performed
            tapAction.onNext(())
            XCTAssertTrue(actionTitle == "Filter")
        }
        XCTAssertFalse(isDatePickerShown)
    }
    
    func testNavigateToDetailScreenOnRowSelection() {
        
        autoreleasepool {
            let viewDidLoad = PublishSubject<Void>()
            let cellDidSelectAtRow = PublishSubject<Int>()
            
            let input = CreateMovieListViewModelInput(
                viewDidLoad: viewDidLoad.asSignal(onErrorJustReturn: ()),
                movieDidSelectAtIndex: cellDidSelectAtRow.asDriverOnErrorJustComplete())
            
            let output = viewModel.transform(input: input)
            
            
            _ = output.reloadTableView.emit()
            _ = output.movieDidSelect.drive()
            
            viewDidLoad.onNext(())
            cellDidSelectAtRow.onNext(0)
            
            XCTAssertTrue(mockNavigator.isNavigate)
            
        }
    }
}
