# MovieSeeker

App will provide latest movies, filter movies for specific date, choose and tap any movie to view its detail. 


# Instalation
Using terminal run following command:

```
$ carthage update --platform iOS
```

# Features
- Latest Movies listing
- Movies filtered by Date
- Detail View of a movie

# User Experience
- On launching the app user will be presented with first page of latest movies
- On reaching to the last movie, next page will be automatically fetched for the user
- Filter button can be used to filter movies for a specific date
- Upon selection of a movie its detail will be presented to the user

# Architecutre
- MVVM + Rx + Navigator

# Test Coverage
- 76% Test coverage provided for ViewModel (Business Logic)

# Dependency
- RxSwift
- Alamofire
- KingFisher

# Author

* **Faizan Ellahi** 
